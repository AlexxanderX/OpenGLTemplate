package net.alexxanderx.opengltemplate;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by alexxanderx on 9/7/15.
 */
public class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;
    private Context mContext;

    public MyGLSurfaceView(Context context) {
        super(context);

        mContext = context;

        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new MyGLRenderer(context);
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
}
