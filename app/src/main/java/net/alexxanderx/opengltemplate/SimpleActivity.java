package net.alexxanderx.opengltemplate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SimpleActivity extends AppCompatActivity {

    private MyGLSurfaceView mGLView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGLView = new MyGLSurfaceView(this);
        setContentView(mGLView);
    }
}
